﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DropDownMenu : MonoBehaviour
{
	//Scripts Ayuda
	GenericUI_ODA09 genericUIScript;
	Menu_ODA09 menuScript;

	//Prebaf
    GameObject dropDownMenuGameObject;
    Boolean openMenu;
    
	//Animacion
	private Animator menuAnimator;

	//Botones
	Button toggleMenuButton, closePopUpButton;


    //private GameObject popUpContainterGameObject;
    GameObject imgFichaTecnicaGameObject, imgCreditosGameObject, imgGlosarioGameObject;

    Button fichaTecnicaButton, creditosButton, glosarioButton, inicioButton;

	// Use this for initialization
	void Start () {
	
		//Script de ayuda
		genericUIScript = transform.GetComponent<GenericUI_ODA09>();
		menuScript = this.transform.GetComponent<Menu_ODA09>();

		dropDownMenuGameObject = this.transform.FindChild("genericUI").Find("dropDownMenu").gameObject;
        toggleMenuButton = dropDownMenuGameObject.transform.GetChild(0).GetComponent<Button>();
	    menuAnimator = dropDownMenuGameObject.transform.GetChild(0).GetChild(0).GetComponent<Animator>();
	    closePopUpButton = dropDownMenuGameObject.transform.GetChild(1).GetComponent<Button>();
	    imgFichaTecnicaGameObject = closePopUpButton.transform.GetChild(0).GetChild(0).gameObject;
        imgCreditosGameObject = closePopUpButton.transform.GetChild(0).GetChild(1).gameObject;
        imgGlosarioGameObject = closePopUpButton.transform.GetChild(0).GetChild(2).gameObject;
	    fichaTecnicaButton = menuAnimator.transform.GetChild(0).GetComponent<Button>();
        creditosButton = menuAnimator.transform.GetChild(1).GetComponent<Button>();
        glosarioButton = menuAnimator.transform.GetChild(2).GetComponent<Button>();
		inicioButton = menuAnimator.transform.GetChild(3).GetComponent<Button>();
        toggleMenuButton.onClick.AddListener(ToggleMenu);
        closePopUpButton.onClick.AddListener(ClosePopUps);

        fichaTecnicaButton.onClick.AddListener(delegate () {
            OpenPopUp("Ficha_Tecnica");
        });
        creditosButton.onClick.AddListener(delegate () {
            OpenPopUp("Creditos");
        });
        glosarioButton.onClick.AddListener(delegate () {
            OpenPopUp("Glosario");
        });
		inicioButton.onClick.AddListener(delegate () {
			GoToInicio();
		});


    }

	void Update(){
		if (genericUIScript.gameUI.activeSelf || menuScript.retos[3].activeSelf) {
			inicioButton.gameObject.SetActive (true);
		} else {
			inicioButton.gameObject.SetActive (false);
		}
	}

	private void GoToInicio()
	{
		genericUIScript.irMenu ();
		ToggleMenu();
	}

    private void OpenPopUp(string popUpName)
    {

        closePopUpButton.gameObject.SetActive(true);
        if (popUpName == "Ficha_Tecnica")
        {
            imgFichaTecnicaGameObject.SetActive(true);
        }
        else if (popUpName == "Creditos")
        {
            imgCreditosGameObject.SetActive(true);
        }
        else if (popUpName == "Glosario")
        {
            imgGlosarioGameObject.SetActive(true);
        }



    }


    void ClosePopUps()
    {
        imgFichaTecnicaGameObject.SetActive(false);
        imgCreditosGameObject.SetActive(false);
        imgGlosarioGameObject.SetActive(false);
        closePopUpButton.gameObject.SetActive(false);
    }


    void ToggleMenu()
    { 
        openMenu = !openMenu;
        menuAnimator.SetBool("Opened", openMenu);
    }

}
