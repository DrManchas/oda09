﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GenericUI_ODA09 : MonoBehaviour {

	//Scripts Ayuda
	Reto1_ODA09 reto1Script;
	Reto2_ODA09 reto2Script;
	Reto3_ODA09 reto3Script;
	RetoFinal_ODA09 retoFinalScript;
	[HideInInspector]
	public SoundManager soundManager;

	//Static
	//[HideInInspector]
	//public GameObject instanciaODA;

	//Contenedores
	[HideInInspector]
	public GameObject genericUI, gameUI, marcador, contador;

	//BlockScreens
	GameObject imgBlockScreen;

	//Botones
	Button btnPausa, btnAyuda;

	//PopUps
	GameObject popUpInfo, popUp2Button;

	//Botones popUp
	Button btnAceptar, btnSi, btnNo;

	//Textos popUp
	Text txtInfo, txt2Button;

	//Textos
	Text txtTiempo;

	//Variables de retos
	[HideInInspector]
	public bool paused = true;
	[HideInInspector]
	public string ayuda = "disponible";
	[HideInInspector]
	public float time = 0.0f;


	// Use this for initialization
	void Start () {

		//Scripts Ayuda
		reto1Script = transform.GetComponent<Reto1_ODA09>();
		reto2Script = transform.GetComponent<Reto2_ODA09>();
		reto3Script = transform.GetComponent<Reto3_ODA09>();
		retoFinalScript = transform.GetComponent<RetoFinal_ODA09>();
		soundManager = transform.GetComponent<SoundManager>();

		//Instancia ODA
		//instanciaODA = Instantiate<GameObject>(this.gameObject);
		//instanciaODA.SetActive (false);

		//Contenedores
		genericUI = transform.FindChild("genericUI").gameObject;
		gameUI = genericUI.transform.FindChild("gameUI").gameObject;
		marcador = gameUI.transform.FindChild("marcador").gameObject;
		contador = gameUI.transform.FindChild("contador").gameObject;

		//BlockScreens
		imgBlockScreen = genericUI.transform.FindChild("imgBlockScreen").gameObject;

		//Botones
		btnPausa = gameUI.transform.FindChild("btnPausa").GetComponent<Button>();
		btnAyuda = gameUI.transform.FindChild("btnAyuda").GetComponent<Button>();

		//PopUps
		popUpInfo = imgBlockScreen.transform.FindChild("popUpInfo").gameObject;
		popUp2Button = imgBlockScreen.transform.FindChild("popUp2Button").gameObject;

		//Botones popUp
		btnAceptar = popUpInfo.transform.FindChild("btnAceptar").GetComponent<Button>();
		btnSi = popUp2Button.transform.FindChild("btnSi").GetComponent<Button>();
		btnNo = popUp2Button.transform.FindChild("btnNo").GetComponent<Button>();

		//Texttos popUP
		txtInfo = popUpInfo.transform.FindChild("txtInfo").GetComponent<Text>();
		txt2Button = popUp2Button.transform.FindChild("txt2Button").GetComponent<Text>();

		//Textos
		txtTiempo = contador.transform.FindChild("txtTiempo").GetComponent<Text>();

		//Funcion botones
		btnPausa.onClick.AddListener (delegate() {
			openPopUp("popUpPausa");
		});

		btnAyuda.onClick.AddListener (delegate() {
			openPopUp("popUpAyuda");
		});
			
		soundManager.playSound ("Tropical_Jazz_Paradise", 0);
	}

	//Funciones secuencia
	public IEnumerator eventosUI (string evento)
	{
		yield return new WaitForSeconds (0.01f);
		string[] instrucciones = evento.Split ('_');
		switch (instrucciones [0]) {
		case "openPopUp":
			openPopUp (instrucciones [1]);
			break;
		case "closePopUp":
			closePopUp (instrucciones [1]);
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
		if (txtTiempo.IsActive() && !paused) {
			time = time + Time.deltaTime;
			txtTiempo.text = string.Format("{0:0}:{1:00}", Mathf.Floor(time / 60), (time % 60));
		}
	}

	void openPopUp (string popUpName)
	{
		imgBlockScreen.SetActive (true);
		switch (popUpName) {
		case "popUpInfo":
			popUpInfo.gameObject.SetActive (true);
			break;
		case "popUp2Button":
			popUp2Button.gameObject.SetActive (true);
			break;
		case "popUpPausa":
			paused = true;
			mostrarInfoPopUp (
				"Pausa",
				"closePopUp_popUpPausa",
				0, 
				0.01f
			);
			break;
		case "popUpAyuda":
			if (ayuda == "usada") {
				paused = true;
				mostrarInfoPopUp (
					"Toca el cubo con el número que permite seguir con la sucesión iniciada.",
					"closePopUp_popUpAyuda",
					0, 
					0.01f
				);
			} else {
				ayuda = "solicitada";
				imgBlockScreen.SetActive (false);
			}
			break;
		}

		soundManager.playSound ("Sound_OpenPopUp_10",1);

	}

	void closePopUp (string popUpName)
	{
		switch(popUpName){
		case "popUpInfo":
			popUpInfo.gameObject.SetActive (false);
			break;
		case "popUp2Button":
			popUp2Button.gameObject.SetActive (false);
			break;
		case "popUpPausa":
			paused = false;
			popUpInfo.gameObject.SetActive (false);
			break;
		case "popUpAyuda":
			paused = false;
			popUpInfo.gameObject.SetActive (false);
			break;
		}
		imgBlockScreen.SetActive (false);
		soundManager.playSound ("Sound_Cerrar_3",1);

	}

	public void irMenu()
	{
		closePopUp ("popUpInfo");
		closePopUp ("popUp2Button");
		time = 0.0f;
		gameUI.SetActive (false);
		this.GetComponent<Menu_ODA09> ().abrirMenu ();
		soundManager.playSound ("Tropical_Jazz_Paradise", 0);
		/*instanciaODA.SetActive (true);
		instanciaODA.GetComponent<Menu_ODA09> ().abrirMenu ();
		Destroy (this.gameObject);*/
	}

	public void reiniciarEscena(int reto, int escena)
	{
		closePopUp ("popUpInfo");
		closePopUp ("popUp2Button");
		//instanciaODA.SetActive (true);

		switch(reto)
		{
		case 1:
			reto1Script.avanzarEscena (escena);//instanciaODA.GetComponent<Reto1_ODA09>().avanzarEscena (escena);
			break;
		case 2:
			reto2Script.avanzarEscena (escena);//instanciaODA.GetComponent<Reto2_ODA09>().avanzarEscena (escena);
			break;
		case 3:
			reto3Script.avanzarEscena (escena);//instanciaODA.GetComponent<Reto3_ODA09>().avanzarEscena (escena);
			break;
		}

		//Destroy (this.gameObject);
	}

	//Muestra popUp de información y asigna la funcion indicada en el boton, del reto indicado
	public void mostrarInfoPopUp(string texto, string funcion, int reto, float tiempoEspera)
	{
		//Habilita los gameObject del popUp
		imgBlockScreen.SetActive (true);
		popUpInfo.SetActive (true);
		soundManager.playSound ("Sound_OpenPopUp_10",1);

		//Asigna el texto al popUp
		txtInfo.text = texto;

		//Remueve todas las funciones anteriores del boton
		btnAceptar.onClick.RemoveAllListeners ();

		switch(reto)
		{
		case 0:
			btnAceptar.onClick.AddListener (delegate() {
				StartCoroutine(eventosUI(funcion));
				closePopUp ("popUpInfo");
			});
			break;
		case 1:
			btnAceptar.onClick.AddListener (delegate() {
				StartCoroutine(reto1Script.secuenciaEventos(funcion,tiempoEspera));
				closePopUp("popUpInfo");
			});
			break;
		case 2:
			btnAceptar.onClick.AddListener (delegate() {
				StartCoroutine(reto2Script.secuenciaEventos(funcion,tiempoEspera));
				closePopUp("popUpInfo");
			});
			break;
		case 3:
			btnAceptar.onClick.AddListener (delegate() {
				StartCoroutine(reto3Script.secuenciaEventos(funcion,tiempoEspera));
				closePopUp("popUpInfo");
			});
			break;
		}

	}
		
	//Muestra popUp de 2 botones, asigna el texto y las funciones indicadas del reto indicado
	public void mostrar2ButtonPopUp(string texto, string funcNo, string funcSi, string txtBtnNo, string txtBtnSi, int reto)
	{
		//Habilita los gameObject del popUp
		imgBlockScreen.SetActive (true);
		popUp2Button.SetActive (true);


		//Asigna el texto al popUp
		txt2Button.text = texto;

		//Texto de los botones
		btnSi.transform.GetChild(0).GetComponent<Text>().text = txtBtnSi;
		btnNo.transform.GetChild(0).GetComponent<Text>().text = txtBtnNo;

		//Remueve todas las funciones anteriores del boton
		btnSi.onClick.RemoveAllListeners ();
		btnNo.onClick.RemoveAllListeners ();

		switch(reto)
		{
		case 1:
			btnSi.onClick.AddListener (delegate() {
				StartCoroutine(reto1Script.secuenciaEventos(funcSi, 1.0f));
				closePopUp("popUp2Button");
			});

			btnNo.onClick.AddListener (delegate() {
				StartCoroutine(reto1Script.secuenciaEventos(funcNo, 1.0f));
				closePopUp("popUp2Button");
			});
			break;
		case 2:
			btnSi.onClick.AddListener (delegate() {
				StartCoroutine(reto2Script.secuenciaEventos(funcSi, 1.0f));
				closePopUp("popUp2Button");
			});

			btnNo.onClick.AddListener (delegate() {
				StartCoroutine(reto2Script.secuenciaEventos(funcNo, 1.0f));
				closePopUp("popUp2Button");
			});
			break;
		case 3:
			btnSi.onClick.AddListener (delegate() {
				StartCoroutine(reto3Script.secuenciaEventos(funcSi, 1.0f));
				closePopUp("popUp2Button");
			});

			btnNo.onClick.AddListener (delegate() {
				StartCoroutine(reto3Script.secuenciaEventos(funcNo, 1.0f));
				closePopUp("popUp2Button");
			});
			break;
		case 4:
			btnSi.onClick.AddListener (delegate() {
				StartCoroutine(retoFinalScript.secuenciaEventos(funcSi, 1.0f));
				closePopUp("popUp2Button");
			});

			btnNo.onClick.AddListener (delegate() {
				StartCoroutine(retoFinalScript.secuenciaEventos(funcNo,1.0f));
				closePopUp("popUp2Button");
			});
			break;
		}
	}


	/*public void setNuevaInstancia()
	{
		Destroy (instanciaODA);
		instanciaODA = Instantiate<GameObject>(this.gameObject);
		instanciaODA.SetActive (false);
	}*/
}
