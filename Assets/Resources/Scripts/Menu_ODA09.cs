﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Menu_ODA09 : MonoBehaviour {

	//Scripts Ayuda
	GenericUI_ODA09 genericUIScript;

	//This obj
	[HideInInspector]
	public GameObject menu;
	public List<GameObject> retos = new List<GameObject>();


	//Botones
	Button btnReto1, btnReto2, btnReto3, btnRetoFinal;

	void Awake(){

		//Script de ayuda
		genericUIScript = transform.GetComponent<GenericUI_ODA09>();

		//Pantallas
		menu = this.transform.FindChild("menu").gameObject;
		retos.Add (this.transform.FindChild ("reto1").gameObject); 
		retos.Add (this.transform.FindChild ("reto2").gameObject);
		retos.Add (this.transform.FindChild ("reto3").gameObject);
		retos.Add (this.transform.FindChild ("retoFinal").gameObject); 
	}

	// Use this for initialization
	void Start () {

		//Asignación de camara
		for (int index = 0; index < retos.Count; index++) {
			retos[index].GetComponent<Canvas> ().worldCamera = Camera.main;
		}

		//Botones
		btnReto1 = menu.transform.FindChild("btnReto1").GetComponent<Button>();
		btnReto2 = menu.transform.FindChild("btnReto2").GetComponent<Button>();
		btnReto3 = menu.transform.FindChild("btnReto3").GetComponent<Button>();
		btnRetoFinal = menu.transform.FindChild("btnRetoFinal").GetComponent<Button>();

		//Funciones boton
		btnReto1.onClick.AddListener (delegate() {
			abrirReto(1);
		});

		btnReto2.onClick.AddListener (delegate() {
			abrirReto(2);
		});

		btnReto3.onClick.AddListener (delegate() {
			abrirReto(3);
		});

		btnRetoFinal.onClick.AddListener (delegate() {
			abrirReto(4);
		});

	}

	//Funcion botones elegir reto
	public void abrirReto(int reto){

		//genericUIScript.soundManager.playSound ("Tropical Jazz Paradise", 1);
		genericUIScript.gameUI.SetActive (true);
		menu.SetActive (false);
		desactivarRetos ();

		switch (reto) 
		{
		case 1:
			retos[0].SetActive (true);
			this.GetComponent<Reto1_ODA09> ().avanzarEscena (1);
			break;
		case 2:
			retos[1].SetActive (true);
			this.GetComponent<Reto2_ODA09> ().avanzarEscena (1);
			break;
		case 3:
			retos[2].SetActive (true);
			this.GetComponent<Reto3_ODA09> ().avanzarEscena (1);
			break;
		case 4:
			retos[3].SetActive (true);
			genericUIScript.genericUI.SetActive (false);
			genericUIScript.gameUI.SetActive (false);
			StartCoroutine(this.GetComponent<RetoFinal_ODA09> ().secuenciaEventos ("iniciarEvaluacion",0.0f));
			break;
		}
			
	}

	public void abrirMenu()
	{
		desactivarRetos ();
		menu.SetActive (true);
		genericUIScript.genericUI.SetActive (true);
		genericUIScript.gameUI.SetActive (false);
	}

	void desactivarRetos()
	{
		for (int index = 0; index < retos.Count; index++) {
			retos[index].SetActive (false);
		}	
	}
}
