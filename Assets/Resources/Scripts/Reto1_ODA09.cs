﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Reto1_ODA09 : MonoBehaviour
{

	//Scripts ayuda
	GenericUI_ODA09 genericUIScript;
	Menu_ODA09 menuScript;


	//Pantallas
	List<GameObject> escenas = new List<GameObject>();

	//BlockScreens
	GameObject imgBlockScreen;

	//popUps
	//Button popUpCollar;

	//ObjetosDeReto
	GameObject figura, jugador, cubo;
	ObjCubo cuboActual;
	List<ObjCubo> cubos = new List<ObjCubo>();

	//Numeros usados al generar piramide
	int numSec = 0;
	int maxSec = 0;
	int maxNum = 0;

	//Variables reto
	int numErrores = 0;
	int ultimaFila = 0;
	int escenaActual = 0;

	// ------------------------Genericos Unity----------------------
	void Start ()
	{

		//Scripts de Ayuda
		genericUIScript = this.transform.GetComponent<GenericUI_ODA09>();
		menuScript = this.transform.GetComponent<Menu_ODA09>();


		//Escenas
		escenas.Add(menuScript.retos[0].transform.FindChild ("Escena1").gameObject);
		escenas.Add(menuScript.retos[0].transform.FindChild ("Escena2").gameObject);
		escenas.Add(menuScript.retos[0].transform.FindChild ("Escena3").gameObject);
		escenas.Add(menuScript.retos[0].transform.FindChild ("Escena4").gameObject);

		//BlockScreens
		imgBlockScreen = escenas[2].transform.FindChild ("imgBlockScreen").gameObject;

		//popUps
		//popUpCollar = imgBlockScreen.transform.FindChild ("popUpCollar").GetComponent<Button>();
	}

	void Update(){

		//Posicion del jugador
		if (jugador != null && cuboActual != null) 
		{
			jugador.transform.localPosition = new Vector3(
				cuboActual.GetCubo().transform.localPosition.x,
				cuboActual.GetCubo().transform.localPosition.y+ 80.0f,
				3.0f
			);
		}
		//Cubo de ayuda
		if(menuScript.retos[0].activeSelf && genericUIScript.ayuda == "solicitada"){
			genericUIScript.ayuda = "usada";
			mostrarSiguiente ();
		}

	}

	// ------------------------Genericos Retos----------------------

	//Funciones secuencia
	public IEnumerator secuenciaEventos (string secuencia, float tiempoEspera)
	{
		yield return new WaitForSeconds (tiempoEspera);
		string[] instrucciones = secuencia.Split ('_');
		int escena;
		switch (instrucciones[0]) {
		case "openPopUp":
			openPopUp (instrucciones[1]);
			break;
		case "closePopUp":
			closePopUp (instrucciones[1]);
			break;
		case "avanzarEscena":
			if (int.TryParse (instrucciones [1], out escena)) {
				avanzarEscena (escena);
			} else {
				menuScript.abrirMenu ();
			}
			break;
		case "reiniciarEscena":
			if (int.TryParse (instrucciones [1], out escena)) {
				genericUIScript.reiniciarEscena(1,escena);;
			} else {
				genericUIScript.irMenu();
			}
			break;
		}
	}

	//Eventos al  avanzarEscena
	public void avanzarEscena (int escena)
	{
		numErrores = 0;
		genericUIScript.ayuda = "disponible";
		genericUIScript.time = 0.0f;
		genericUIScript.paused = true;
		escenaActual = escena;
		imgBlockScreen.SetActive (false);

		if (figura != null) {
			limpiarFigura ();
		}

		switch (escena) {
		case 1:
			cerrarOtrasEscenas ();
			genericUIScript.gameUI.SetActive (false);
			StartCoroutine (secuenciaEventos ("openPopUp_cuboPerdido", 3.0f));
			break;
		case 2:
			cerrarOtrasEscenas ();
			genericUIScript.gameUI.SetActive (true);
			StartCoroutine (secuenciaEventos ("openPopUp_instruccionesReto", 1.20f));
			figura = escenas [1].transform.FindChild ("piramide").gameObject;
			jugador = escenas [1].transform.FindChild ("imgJugador").gameObject;
			ultimaFila = 7;
			generarPiramide ();
			maxSec = 9;
			maxNum = 90;
			generarCamino ();
			break;
		case 3:
			cerrarOtrasEscenas ();
			genericUIScript.genericUI.SetActive (true);
			StartCoroutine (secuenciaEventos ("openPopUp_instruccionesReto2", 1.20f));
			figura = escenas [2].transform.FindChild ("hexagono").gameObject;
			jugador = escenas [2].transform.FindChild ("imgJugador").gameObject;
			ultimaFila = 9;
			generarHexagono ();
			maxSec = 10;
			maxNum = 120;
			generarCamino ();
			break;
		case 4:
			cerrarOtrasEscenas ();
			genericUIScript.gameUI.SetActive (false);
			StartCoroutine (secuenciaEventos("openPopUp_terminarReto", 3.0f));
			break;
		case 5:
			menuScript.abrirReto (2);
			break;
		}
		//genericUIScript.setNuevaInstancia ();
	}

	//Eventos al abrir popUp

	public void openPopUp (string popUpName)
	{

		switch(popUpName){
		case "cuboPerdido":
			genericUIScript.mostrarInfoPopUp (
				"¡Cubo se perdió! Si resolvemos las siguientes pistas, seguro que lo encontraremos.",
				"closePopUp_cuboPerdido",
				1,
				1.20f
			);
			break;
		case "instruccionesReto":
			genericUIScript.mostrarInfoPopUp (
				"Toca los números que continúan la sucesión para poder avanzar.",
				"closePopUp_instruccionesReto",
				1, 
				1.20f
			);
			break;
		case "instruccionesReto2":
			genericUIScript.mostrarInfoPopUp (
				"Toca el cubo con el número que permite seguir con la sucesión iniciada.",
				"closePopUp_instruccionesReto",
				1, 
				1.20f
			);
			break;
		case "perderJuego":
			genericUIScript.mostrarInfoPopUp (
				"Te equivocaste de camino varias veces, vuelve a intentarlo. Cubo te está esperando.",
				"closePopUp_perderJuego",
				1, 
				1.20f
			);
			break;
		case "ganarJuegoE2":
			genericUIScript.paused = true;
			genericUIScript.mostrarInfoPopUp (
				"¡Bien hecho! Tardaste "+
				(string.Format("{0:0}:{1:00}", Mathf.Floor(genericUIScript.time / 60), (genericUIScript.time % 60)))+
				" en resolver esta sucesión, veamos si puedes mejorar este récord.",
				"closePopUp_ganarJuegoE2",
				1, 
				1.20f
			);
			break;
		case "ganarJuegoE3":
			genericUIScript.paused = true;
			imgBlockScreen.SetActive (true);
			genericUIScript.soundManager.playSound ("ganar_pista",1);
			StartCoroutine (secuenciaEventos("closePopUp_popUpCollar", 2.0f));
			break;
		case "terminarReto":
			genericUIScript.soundManager.playSound ("TerminarReto",1);
			genericUIScript.mostrar2ButtonPopUp (
				"Este collar es una pista, Cubo estuvo aquí. Si quieres superar tu récord, puedes volver a intentar el reto, " +
				"si no, continuemos nuestra búsqueda.",
				"avanzarEscena_5",
				"reiniciarEscena_1",
				"RETO 2",
				"VOLVER A INTENTAR",
				1
			);
			break;
		}

	}

	//Eventos al cerrar popUp
	void closePopUp (string popUpName)
	{
		switch(popUpName){
		case "cuboPerdido":
			StartCoroutine (secuenciaEventos("avanzarEscena_2", 1.20f));
			break;
		case "instruccionesReto":
			genericUIScript.time = 0.0f;
			genericUIScript.paused = false;
			cuboActual.GetCubo().transform.GetComponent<Animator>().SetInteger("estado", 2);
			//Habilita siguientes
			foreach (ObjCubo siguientes in cuboActual.GetAdyacentes()){
				siguientes.GetCubo().transform.GetComponent<Button> ().interactable = true;
			}
			mostrarSiguiente ();
			break;
		case "perderJuego":
			limpiarFigura ();
			genericUIScript.reiniciarEscena(1,escenaActual);
			break;
		case "ganarJuegoE2":
			limpiarFigura ();
			avanzarEscena (3);
			break;
		case "popUpCollar":
			limpiarFigura ();
			imgBlockScreen.SetActive (false);
			avanzarEscena (4);
			break;
		}

	}

	void cerrarOtrasEscenas()
	{
		for (int index = 0; index < escenas.Count; index++) {
			if (index + 1 == escenaActual) {
				escenas[index].SetActive (true);
			} else {
				escenas[index].SetActive (false);
			}
		}
	}

	// ------------------------Individuales Reto----------------------

	//Generador de figura
	void generarPiramide(){
		try{
			if (cubos.Count > 0) {
				foreach (ObjCubo cubo in cubos) {
					cubo.SetEstado ("");
				}
			} else {
				//Obtiene el cubo
				cubo = figura.transform.FindChild ("cubo").gameObject;
				ObjCubo objCubo = new ObjCubo (cubo, 1, 1, "");
				cubos.Add (objCubo);
				//Genera piramide en base al primer cubo
				for (int fila = 2; fila <= ultimaFila; fila++) {
					for (int columna = 1; columna <= fila; columna++) {
						GameObject clone = Instantiate (cubo);
						clone.transform.SetParent (figura.transform);
						clone.transform.localScale =  new Vector3(1.0f, 1.0f, 1.0f);
						clone.transform.localPosition = new Vector3(
							cubo.transform.localPosition.x + ((columna * 115) - ((fila * 115)/2)) - 57.5f,
							cubo.transform.localPosition.y - (78.0f * (fila-1)),
							cubo.transform.localPosition.z
						);
						objCubo = new ObjCubo (clone, fila, columna, "");
						cubos.Add (objCubo);
					}
				}

				//Definir adyacentes
				for (int index = 0; index < cubos.Count; index++){

					ObjCubo cuboA = cubos [index];
					if (cuboA.GetColumna() != 1) 
					{
						cuboA.GetAdyacentes().Add (cubos [index-1]);
					}

					if (cuboA.GetColumna() != cuboA.GetFila()) 
					{
						cuboA.GetAdyacentes().Add (cubos [index+1]);
					}
					if (cuboA.GetFila() != ultimaFila) 
					{
						int cuboSigFila = (cuboA.GetFila()) + index;
						cuboA.GetAdyacentes().Add (cubos[cuboSigFila]);
						cuboA.GetAdyacentes().Add (cubos[cuboSigFila+1]);
					}

				}
			}
		} catch {
			Debug.Log ("No se pudo generar la piramide");
		}
	}

	//Generador de figura
	void generarHexagono(){

		try{
			if (cubos.Count > 0) {
				foreach (ObjCubo cubo in cubos) {
					cubo.SetEstado ("");
				}
			} else {
				//Obtiene el cubo
				cubo = figura.transform.FindChild ("cubo").gameObject;
				ObjCubo objCubo = new ObjCubo (cubo, 1, 1, "");
				cubos.Add (objCubo);
				//Genera piramide en base al primer cubo
				for (int fila = 1; fila <= ultimaFila; fila++) {
					int maxColumna = 0;
					if (fila == 1 || fila == 9) {
						maxColumna = 2;
					} else if (fila == 2 || fila == 8) {
						maxColumna = 3;
					} else if (fila == 3 || fila == 7 || fila == 5) {
						maxColumna = 6;
					} else if (fila == 4 || fila == 6) {
						maxColumna = 7;
					}
					int posicionInicial = -((maxColumna * 92)/2);
					for (int columna = 1; columna <= maxColumna; columna++) {
						if (columna != 1 || fila != 1) {
							GameObject clone = Instantiate (cubo);
							clone.transform.SetParent (figura.transform);
							clone.transform.localScale =  new Vector3(1.0f, 1.0f, 1.0f);
							clone.transform.localPosition = new Vector3(
								posicionInicial + ((columna * 92) - 46f),
								cubo.transform.localPosition.y - (62.0f * (fila-1)),
								cubo.transform.localPosition.z
							);
							objCubo = new ObjCubo (clone, fila, columna, "");
							cubos.Add (objCubo);
						}
					}
				}

				//Definir adyacentes
				for (int index = 0; index < cubos.Count; index++){

					ObjCubo cuboA = cubos [index];

					if (cuboA.GetColumna() != 1 && !(cuboA.GetColumna() == 2 && cuboA.GetFila() == 7)) 
					{
						cuboA.GetAdyacentes().Add (cubos [index-1]);
					}


					int maxColumna = 0;
					if (cuboA.GetFila() == 1 || cuboA.GetFila() == 9) {
						maxColumna = 2;
					} else if (cuboA.GetFila() == 2 || cuboA.GetFila() == 8) {
						maxColumna = 3;
					} else if (cuboA.GetFila() == 3 || cuboA.GetFila() == 7 || cuboA.GetFila() == 5) {
						maxColumna = 6;
					} else if (cuboA.GetFila() == 4 || cuboA.GetFila() == 6) {
						maxColumna = 7;
					}

					if (cuboA.GetColumna() != maxColumna && !(cuboA.GetColumna() == 5 && cuboA.GetFila() == 7)) 
					{
						cuboA.GetAdyacentes().Add (cubos [index+1]);
					}

					switch(cuboA.GetFila()){
					case 1:
						cuboA.GetAdyacentes().Add (cubos[index+2]);
						cuboA.GetAdyacentes().Add (cubos[index+3]);
						break;
					case 2:
						cuboA.GetAdyacentes().Add (cubos[index+4]);
						cuboA.GetAdyacentes().Add (cubos[index+5]);
						break;
					case 3:
						cuboA.GetAdyacentes().Add (cubos[index+6]);
						cuboA.GetAdyacentes().Add (cubos[index+7]);
						break;
					case 4:
						if (cuboA.GetColumna () == 1) {
							cuboA.GetAdyacentes().Add (cubos[index+7]);
						} else if (cuboA.GetColumna () == 7) {
							cuboA.GetAdyacentes().Add (cubos[index+6]);
						} else {
							cuboA.GetAdyacentes().Add (cubos[index+6]);
							cuboA.GetAdyacentes().Add (cubos[index+7]);
						}
						break;
					case 5:
						cuboA.GetAdyacentes().Add (cubos[index+6]);
						cuboA.GetAdyacentes().Add (cubos[index+7]);
						break;
					case 6:
						if (cuboA.GetColumna () == 1) {
							cuboA.GetAdyacentes().Add (cubos[index+7]);
						} else if (cuboA.GetColumna () == 7) {
							cuboA.GetAdyacentes().Add (cubos[index+6]);
						} else {
							cuboA.GetAdyacentes().Add (cubos[index+6]);
							cuboA.GetAdyacentes().Add (cubos[index+7]);
						}
						break;
					case 7:
						if (cuboA.GetColumna () == 2) {
							cuboA.GetAdyacentes().Add (cubos[index+5]);
						} else if (cuboA.GetColumna () > 2 && cuboA.GetColumna () < 5){
							cuboA.GetAdyacentes().Add (cubos[index+4]);
							cuboA.GetAdyacentes().Add (cubos[index+5]);
						} else if (cuboA.GetColumna () == 5){
							cuboA.GetAdyacentes().Add (cubos[index+4]);
						}
						break;
					case 8:
						if (cuboA.GetColumna () == 1) {
							cuboA.GetAdyacentes().Add (cubos[index+3]);
						} else if (cuboA.GetColumna () == 3) {
							cuboA.GetAdyacentes().Add (cubos[index+2]);
						} else {
							cuboA.GetAdyacentes().Add (cubos[index+2]);
							cuboA.GetAdyacentes().Add (cubos[index+3]);
						}
						break;
					}
				}
			}
		} catch {
			Debug.Log ("No se pudo generar el hexagono");
		}
	}

	void limpiarFigura(){

		try{
			cubos.Clear ();
			cuboActual = null;
			figura.transform.GetChild(0).GetComponent<Animator> ().SetInteger ("estado", 0);
			for(int index = 1 ; index < figura.transform.childCount; index++){
				Destroy(figura.transform.GetChild(index).gameObject);
			}
		} catch {
			Debug.Log ("No se pudo limpiar la Figura");
		}

	}

	void generarCamino(){
		try{
			//Secuencia random
			int numNextSec = Random.Range(2,maxSec);
			int intentos = 0;
			while (numNextSec == numSec) {
				numNextSec = Random.Range(2,maxSec);
				intentos++;
				if(intentos>20){
					print("maximo de intentos en while decidir secuencia");
					break;
				}
			}
			numSec = numNextSec;
			int numSaltos = (maxNum/numSec) - ultimaFila;

			//Asigna primer número
			asignarCaminoCorrecto(cubos[0],1, numSaltos);

			//Habilitar primer cubo
			cuboActual = cubos[0];

		} catch {
			closePopUp ("perderJuego");
			Debug.Log ("No se pudo generar el camino");
		}

	}

	void asignarCaminoCorrecto(ObjCubo actual, int numCubo, int numSaltos)
	{
		try{
			//Asigna el valor de la secuencia
			actual.GetCubo().transform.GetChild(0).GetComponent<Text> ().text = ""+(numSec*numCubo);


			//Agrega funcion de perder al boton correcto
			actual.GetCubo().transform.GetComponent<Button>().onClick.RemoveAllListeners ();
			actual.GetCubo().transform.GetComponent<Button>().onClick.AddListener(delegate() {
				seleccionarSecuencia(actual);
			});
			actual.GetCubo ().transform.GetComponent<Button> ().interactable = false;

			//Estado de número de secuencia correcto
			actual.SetEstado ("correcto");

			//Continua asignando si no es el final de la piramide
			if(actual.GetFila () != ultimaFila){
				numCubo++;
				int randomSiguiente = Random.Range (0, actual.GetAdyacentes ().Count);
				int intentos = 0;
				while (actual.GetAdyacentes()[randomSiguiente].GetEstado() == "correcto"
					|| ((actual.GetFila() == actual.GetAdyacentes()[randomSiguiente].GetFila())
					&& numSaltos == 0)){
					randomSiguiente = Random.Range (0, actual.GetAdyacentes ().Count);
					intentos++;
					if(intentos>20){
						print("maximo de intentos en while asignar Camino Correcto");
						print("numCubo Actual: "+numCubo);
						print("Secuencia Actual: "+numSec);
						print("Saltos Actual: "+numSaltos);
						closePopUp ("perderJuego");
						break;
					}
				}
				if(actual.GetFila() == actual.GetAdyacentes()[randomSiguiente].GetFila()){
					numSaltos--;
				}
				asignarCaminoCorrecto (actual.GetAdyacentes () [randomSiguiente], numCubo, numSaltos);
				for (int index = 0; index < actual.GetAdyacentes ().Count; index++) {
					if (index != randomSiguiente && actual.GetAdyacentes () [index].GetEstado () != "correcto") {
						asignarCaminoInCorrecto (actual.GetAdyacentes () [index], (numSec * numCubo));
					}
				}
			}
		} catch {
			closePopUp ("perderJuego");
			Debug.Log ("Fallo la asignacion de camino correcto");
		}
	}

	void asignarCaminoInCorrecto(ObjCubo actual, int numAnterior)
	{
		try{
			int randomSum = Random.Range (1, 10);
			//Asegurar que número no este en la piramide
			int intentos = 0;
			while ((numAnterior+randomSum) % numSec == 0){
				randomSum = Random.Range (1, 10);
				intentos++;
				if(intentos>20){
					print("maximo de intentos en while asignar Camino Incorrecto");
					closePopUp ("perderJuego");
					break;
				}
					
			}

			//Asignar valor aleatorio parecido al anterior
			actual.GetCubo().transform.GetChild(0).GetComponent<Text> ().text = ""+(numAnterior+randomSum);


			//Estado de número de secuencia incorrecto
			actual.SetEstado ("incorrecto");

			//Agrega funcion de perder al boton incorrecto
			actual.GetCubo().transform.GetComponent<Button>().onClick.AddListener(delegate() {
				seleccionarSecuencia(actual);
			});
			actual.GetCubo ().transform.GetComponent<Button> ().interactable = false;

			//Continua asignando si no es el final de la piramide
			if(actual.GetFila () != ultimaFila){
				for (int index = 0; index < actual.GetAdyacentes ().Count; index++) {
					//Reviza que no tenga ya asignación
					if (actual.GetAdyacentes()[index].GetEstado() == "") {
						asignarCaminoInCorrecto (actual.GetAdyacentes () [index], numAnterior+randomSum);
					} 
				}
			}
		} catch {
			closePopUp ("perderJuego");
			Debug.Log ("Fallo la asignacion de camino incorrecto");
		}
	}

	void seleccionarSecuencia(ObjCubo cuboPresionado)
	{
		try{
			int numCubo, numCuboActual;
			int.TryParse (cuboPresionado.GetCubo().transform.GetChild(0).GetComponent<Text> ().text, out numCubo);
			int.TryParse (cuboActual.GetCubo ().transform.GetChild(0).GetComponent<Text> ().text, out numCuboActual);
			if (numCubo == (numSec + numCuboActual)) {
				cuboActual = cuboPresionado;
				cuboActual.GetCubo().transform.GetComponent<Animator>().SetInteger("estado", 2);
				cuboActual.GetCubo ().transform.GetComponent<Button> ().interactable = false;
				//Jugador gana
				if (cuboPresionado.GetFila () == ultimaFila) {
					StartCoroutine (secuenciaEventos(("openPopUp_ganarJuegoE"+escenaActual),1.2f));
				} else {
					//Habilita siguientes
					foreach (ObjCubo siguiente in cuboPresionado.GetAdyacentes()){
						if(siguiente.GetCubo().transform.GetComponent<Animator>().GetInteger("estado") != 2){
							siguiente.GetCubo().transform.GetComponent<Button> ().interactable = true;
						}
					}
				}
				genericUIScript.soundManager.playSound ("Sound_correcto_10",1);
			} else {
				numErrores++;
				genericUIScript.soundManager.playSound ("Sound_incorrecto_19",1);
				if(numErrores >= 3){
					StartCoroutine (secuenciaEventos("openPopUp_perderJuego",1.2f));
				}
			}
		} catch {
			Debug.Log ("Fallo al seleccionar siguiente cubo");
		}
	}

	void mostrarSiguiente(){
		try{
			int numCuboAdyacente, numCuboActual;
			int.TryParse (cuboActual.GetCubo ().transform.GetChild(0).GetComponent<Text> ().text, out numCuboActual);
			for (int index = 0; index < cuboActual.GetAdyacentes().Count; index++) {
				int.TryParse (cuboActual.GetAdyacentes () [index].GetCubo ().transform.GetChild(0).GetComponent<Text> ().text, out numCuboAdyacente);
				if (numCuboAdyacente == (numSec + numCuboActual)) {
					cuboActual.GetAdyacentes () [index].GetCubo().transform.GetComponent<Animator>().SetInteger("estado", 1);
					break;
				}
			}
		} catch {
			Debug.Log ("Fallo al mostrar el siguiente en la secuencia");
		}
	}
}

public class ObjCubo
{
	GameObject _cubo;
	int _fila;
	int _columna;
	string _estado;
	List<ObjCubo> _adyacentes;

	public GameObject GetCubo()
	{
		return _cubo;
	}

	public int GetFila()
	{
		return _fila;
	}

	public int GetColumna()
	{
		return _columna;
	}

	public string GetEstado()
	{
		return _estado;
	}
		
	public void SetEstado(string estado)
	{
		_estado = estado;
	}

	public List<ObjCubo> GetAdyacentes()
	{
		return _adyacentes;
	}

	/// <summary>
	/// Cubo para formar figura
	/// </summary>
	/// <param name="cubo">GameObject del cubo</param>
	/// <param name="fila">Fila a la que pertenece</param>
	/// <param name="columna">columna a la que pertenece</param>
	/// <param name="estado">estado actual del objeto</param>
	public ObjCubo(GameObject cubo, int fila, int columna, string estado)
	{
		_cubo = cubo;
		_fila = fila;
		_columna = columna;
		_estado = estado;
		_adyacentes = new List<ObjCubo>();
	}
}