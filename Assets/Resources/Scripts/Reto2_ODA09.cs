﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class Reto2_ODA09 : MonoBehaviour {

	//Scripts ayuda
	GenericUI_ODA09 genericUIScript;
	Menu_ODA09 menuScript;


	//Pantallas
	List<GameObject> escenas = new List<GameObject>();

	//BlockScreens
	GameObject imgBlockScreen;

	//ObjetosDeReto
	GameObject figura, jugador;
	ObjBloque bloqueActual;
	List<ObjBloque> bloques = new List<ObjBloque>();

	//Numeros usados al generar piramide
	int numSec = 0;

	//Variables reto
	int escenaActual = 0;

	// Use this for initialization
	void Start ()
	{

		//Scripts de Ayuda
		genericUIScript = this.transform.GetComponent<GenericUI_ODA09>();
		menuScript = this.transform.GetComponent<Menu_ODA09>();


		//Escenas
		escenas.Add(menuScript.retos[1].transform.FindChild ("Escena1").gameObject);
		escenas.Add(menuScript.retos[1].transform.FindChild ("Escena2").gameObject);
		escenas.Add(menuScript.retos[1].transform.FindChild ("Escena3").gameObject);

		//BlockScreens
		imgBlockScreen = escenas[1].transform.FindChild ("imgBlockScreen").gameObject;
			
	}

	void Update(){

		//Posicion del jugador
		if (jugador != null && bloqueActual != null) 
		{
			jugador.transform.localPosition = new Vector3(
				-bloqueActual.GetBloque().transform.localPosition.x,
				bloqueActual.GetBloque().transform.localPosition.y+ 80.0f,
				3.0f
			);
		}
		//Cubo de ayuda
		if(menuScript.retos[1].activeSelf && genericUIScript.ayuda == "solicitada"){
			genericUIScript.ayuda = "usada";
			mostrarSiguiente ();
		}

	}

	// ------------------------Genericos Retos----------------------

	//Funciones secuencia
	public IEnumerator secuenciaEventos (string secuencia, float tiempoEspera)
	{
		yield return new WaitForSeconds (tiempoEspera);
		string[] instrucciones = secuencia.Split ('_');
		int escena;
		switch (instrucciones[0]) {
		case "openPopUp":
			openPopUp (instrucciones[1]);
			break;
		case "closePopUp":
			closePopUp (instrucciones[1]);
			break;
		case "avanzarEscena":
			if (int.TryParse (instrucciones [1], out escena)) {
				avanzarEscena (escena);
			} else {
				menuScript.abrirMenu ();
			}
			break;
		case "reiniciarEscena":
			if (int.TryParse (instrucciones [1], out escena)) {
				genericUIScript.reiniciarEscena(2,escena);;
			} else {
				genericUIScript.irMenu();
			}
			break;
		}
	}

	//Eventos al  avanzarEscena
	public void avanzarEscena (int escena)
	{

		genericUIScript.ayuda = "disponible";
		genericUIScript.time = 0.0f;
		genericUIScript.paused = true;
		escenaActual = escena;

		imgBlockScreen.SetActive (false);

		if (figura != null) {
			limpiarFigura ();
		}

		switch (escena) {
		case 1:
			cerrarOtrasEscenas ();
			genericUIScript.gameUI.SetActive (true);
			StartCoroutine (secuenciaEventos ("openPopUp_instruccionesReto", 1.2f));
			figura = escenas[0].transform.FindChild ("camino1").gameObject;
			jugador = escenas[0].transform.FindChild ("imgJugador").gameObject;
			obtenerCamino1 ();
			generarCamino ();
			break;
		case 2:
			cerrarOtrasEscenas ();
			genericUIScript.gameUI.SetActive (true);
			StartCoroutine (secuenciaEventos ("openPopUp_instruccionesReto", 1.2f));
			figura = escenas[1].transform.FindChild ("camino2").gameObject;
			jugador = escenas[1].transform.FindChild ("imgJugador").gameObject;
			obtenerCamino2 ();
			generarCamino ();
			break;
		case 3:
			cerrarOtrasEscenas ();
			genericUIScript.gameUI.SetActive (false);
			StartCoroutine (secuenciaEventos("openPopUp_terminarReto", 3.0f));
			break;
		case 4:
			menuScript.abrirReto (3);
			break;
		}
	}

	//Eventos al abrir popUp

	public void openPopUp (string popUpName)
	{

		switch(popUpName){
		case "instruccionesReto":
			genericUIScript.mostrarInfoPopUp (
				"Toca los números que continúan la sucesión para poder avanzar.",
				"closePopUp_instruccionesReto",
				2, 
				1.2f
			);
			break;
		case "perderJuego":
			genericUIScript.mostrarInfoPopUp (
				"Éste no es el camino, vuelve a intentarlo. Cubo quiere regresar a casa.",
				"closePopUp_perderJuego",
				2, 
				1.2f
			);
			break;
		case "ganarJuegoE1":
			genericUIScript.paused = true;
			genericUIScript.mostrarInfoPopUp (
				"¡Bien hecho! Tardaste "+
				(string.Format("{0:0}:{1:00}", Mathf.Floor(genericUIScript.time / 60), (genericUIScript.time % 60)))+
				" en resolver esta sucesión, veamos si puedes mejorar este récord. Estamos cada vez más cerca de encontrar a Cubo.",
				"closePopUp_ganarJuegoE1",
				2, 
				1.2f
			);
			break;
		case "ganarJuegoE2":
			genericUIScript.paused = true;
			imgBlockScreen.SetActive (true);
			genericUIScript.soundManager.playSound ("ganar_pista",1);
			StartCoroutine (secuenciaEventos("closePopUp_popUpHueso", 2.0f));
			break;
		case "terminarReto":
			genericUIScript.mostrar2ButtonPopUp (
				"Este hueso es una pista, Cubo estuvo aquí. Si quieres superar tu récord, puedes volver a intentar el reto, si no, " +
				"continuemos nuestra búsqueda.",
				"avanzarEscena_4",
				"reiniciarEscena_1",
				"RETO 3",
				"VOLVER A INTENTAR",
				2
			);
			break;
		}

	}

	//Eventos al cerrar popUp
	void closePopUp (string popUpName)
	{
		switch(popUpName){
		case "bloquePerdido":
			StartCoroutine (secuenciaEventos("avanzarEscena_2", 1.2f));
			break;
		case "instruccionesReto":
			genericUIScript.time = 0.0f;
			genericUIScript.paused = false;
			bloqueActual.GetBloque().transform.GetComponent<Animator>().SetInteger("estado", 2);
			//Habilita siguientes
			foreach (ObjBloque siguientes in bloqueActual.GetAdyacentes()){
				siguientes.GetBloque().transform.GetComponent<Button> ().interactable = true;
			}
			break;
		case "perderJuego":
			limpiarFigura ();
			genericUIScript.reiniciarEscena(2,escenaActual);
			break;
		case "ganarJuegoE1":
			limpiarFigura ();
			avanzarEscena (2);
			break;
		case "popUpHueso":
			limpiarFigura ();
			imgBlockScreen.SetActive (false);
			avanzarEscena (3);
			break;
		}

		genericUIScript.soundManager.playSound ("Sound_Cerrar_3",1);

	}

	void cerrarOtrasEscenas()
	{
		for (int index = 0; index < escenas.Count; index++) {
			if (index + 1 == escenaActual) {
				escenas[index].SetActive (true);
			} else {
				escenas[index].SetActive (false);
			}
		}
	}

	// ------------------------Individuales Reto----------------------

	void obtenerCamino1(){
		try{
			//obtiene camino
			for(int index =1 ; index <= figura.transform.childCount; index++){
				bloques.Add (new ObjBloque (figura.transform.FindChild("bloque"+index).gameObject, ""));
			}

			bloques[0].GetAdyacentes().Add(bloques[1]);
			bloques[0].GetAdyacentes().Add(bloques[2]);

			bloques[1].GetAdyacentes().Add(bloques[3]);

			bloques[2].GetAdyacentes().Add(bloques[3]);

			bloques[3].GetAdyacentes().Add(bloques[4]);
			bloques[3].GetAdyacentes().Add(bloques[5]);

			bloques[4].GetAdyacentes().Add(bloques[6]);
			bloques[4].GetAdyacentes().Add(bloques[7]);

			bloques[5].GetAdyacentes().Add(bloques[6]);
			bloques[5].GetAdyacentes().Add(bloques[7]);

			bloques[6].GetAdyacentes().Add(bloques[8]);

			bloques[7].GetAdyacentes().Add(bloques[8]);

		} catch {
			Debug.Log ("No se pudo obtener el camino 1");
		}

	}

	void obtenerCamino2(){

		try{
			//obtiene camino
			for(int index =1 ; index <= figura.transform.childCount; index++){
				bloques.Add (new ObjBloque (figura.transform.FindChild("bloque"+index).gameObject, ""));
			}

			bloques[0].GetAdyacentes().Add(bloques[1]);
			bloques[0].GetAdyacentes().Add(bloques[2]);

			bloques[1].GetAdyacentes().Add(bloques[3]);

			bloques[2].GetAdyacentes().Add(bloques[3]);

			bloques[3].GetAdyacentes().Add(bloques[4]);
			bloques[3].GetAdyacentes().Add(bloques[5]);

			bloques[4].GetAdyacentes().Add(bloques[6]);

			bloques[5].GetAdyacentes().Add(bloques[6]);

			bloques[6].GetAdyacentes().Add(bloques[7]);
			bloques[6].GetAdyacentes().Add(bloques[8]);

			bloques[7].GetAdyacentes().Add(bloques[9]);

			bloques[8].GetAdyacentes().Add(bloques[9]);
		} catch {
			Debug.Log ("No se pudo obtener el camino 2");
		}
	}

	void limpiarFigura(){
		try{
			foreach (ObjBloque bloq in bloques) {
				bloq.GetBloque ().transform.GetChild (0).GetComponent<Text> ().text = "";
				bloq.GetBloque ().transform.GetComponent<Animator> ().SetInteger ("estado", 0);
			}

			bloques.Clear ();
			bloqueActual = null;

		} catch {
			Debug.Log ("No se pudo limpiar la figura");
		}
	}

	void generarCamino(){

		try{
			//Secuencia random
			int numNextSec = Random.Range(2,5);
			while (numNextSec == numSec) {
				numNextSec = Random.Range(2,5);
			}
			numSec = numNextSec;
			//Asigna primer número
			asignarCaminoCorrecto(bloques[0],1);

			//Habilitar primer bloque
			bloqueActual = bloques[0];

		} catch {
			Debug.Log ("Fallo al generar el camino");
		}

	}

	void asignarCaminoCorrecto(ObjBloque actual, int numBloque)
	{
		try{
			//Asigna el valor de la secuencia
			actual.GetBloque().transform.GetChild(0).GetComponent<Text> ().text = ""+System.Math.Pow(numSec,numBloque);

			//Agrega funcion de perder al boton correcto
			actual.GetBloque().transform.GetComponent<Button>().onClick.RemoveAllListeners ();
			actual.GetBloque().transform.GetComponent<Button>().onClick.AddListener(delegate() {
				seleccionarSecuencia(actual);
			});
			actual.GetBloque ().transform.GetComponent<Button> ().interactable = false;

			//Estado de número de secuencia correcto
			actual.SetEstado ("correcto");

			//Continua asignando si no es el final de la piramide
			if(actual.GetAdyacentes ().Count != 0){
				numBloque++;
				int randomSiguiente = Random.Range (0, actual.GetAdyacentes ().Count);
				asignarCaminoCorrecto (actual.GetAdyacentes () [randomSiguiente], numBloque);
				for (int index = 0; index < actual.GetAdyacentes ().Count; index++) {
					if (index != randomSiguiente) {
						if (actual.GetAdyacentes () [index].GetEstado () != "correcto") {
							asignarCaminoInCorrecto (actual.GetAdyacentes () [index], numBloque);
						}
					}
				}
			}
		} catch {
			Debug.Log ("No se pudo asignar el camino correcto");
		}
	}

	void asignarCaminoInCorrecto(ObjBloque actual, int numBloque)
	{
		try{
			int randomSum = Random.Range (-9, 10);
			int potencia = (int) System.Math.Pow (numSec, numBloque);
			//Asegurar que número no este en la piramide
			while ((potencia+randomSum) % numSec == 0){
				randomSum = Random.Range (-9, 10);
			}

			//Asignar valor aleatorio parecido al anterior
			actual.GetBloque().transform.GetChild(0).GetComponent<Text> ().text = ""+(potencia+randomSum);

			//Estado de número de secuencia incorrecto
			actual.SetEstado ("incorrecto");

			//Agrega funcion de perder al boton incorrecto
			actual.GetBloque().transform.GetComponent<Button>().onClick.AddListener(delegate() {
				seleccionarSecuencia(actual);
			});
			actual.GetBloque ().transform.GetComponent<Button> ().interactable = false;

		} catch {
			Debug.Log ("No se pudo asignar el camino incorrecto");
		}

	}

	void seleccionarSecuencia(ObjBloque bloquePresionado)
	{
		try{
			int numBloque;
			int.TryParse (bloquePresionado.GetBloque().transform.GetChild(0).GetComponent<Text> ().text, out numBloque);
			if (numBloque % numSec == 0) {
				bloqueActual = bloquePresionado;
				bloqueActual.GetBloque().transform.GetComponent<Animator>().SetInteger("estado", 2);
				bloqueActual.GetBloque ().transform.GetComponent<Button> ().interactable = false;
				//Jugador gana
				if (bloquePresionado.GetAdyacentes ().Count == 0) {
					StartCoroutine(secuenciaEventos(("openPopUp_ganarJuegoE"+escenaActual), 1.2f));
				} else {
					//Habilita siguientes
					foreach (ObjBloque siguiente in bloquePresionado.GetAdyacentes()){
						if(siguiente.GetBloque().transform.GetComponent<Animator>().GetInteger("estado") != 2){
							siguiente.GetBloque().transform.GetComponent<Button> ().interactable = true;
						}
					}
				}
				genericUIScript.soundManager.playSound ("Sound_correcto_10",1);
			} else {
				genericUIScript.soundManager.playSound ("Sound_incorrecto_19",1);
				StartCoroutine (secuenciaEventos("openPopUp_perderJuego", 1.2f));
			}
		} catch {
			Debug.Log ("No se pudo seleccionar el siguiente en secuencia");
		}
	}
	//Modificar
	void mostrarSiguiente(){
		try{
			int numBloqueAdyacente;
			for (int index = 0; index < bloqueActual.GetAdyacentes().Count; index++) {
				int.TryParse (bloqueActual.GetAdyacentes () [index].GetBloque ().transform.GetChild(0).GetComponent<Text> ().text, out numBloqueAdyacente);
				print(numBloqueAdyacente);
				if (numBloqueAdyacente % numSec == 0) {
					bloqueActual.GetAdyacentes () [index].GetBloque().transform.GetComponent<Animator>().SetInteger("estado", 1);
					print("estado cambiado");
					break;
				}
			}
		} catch  {
			Debug.Log ("No se pudo mostrar el numero siguiente ");
		}
	}
}

public class ObjBloque
{
	GameObject _bloque;
	string _estado;
	List<ObjBloque> _adyacentes;

	public GameObject GetBloque()
	{
		return _bloque;
	}

	public string GetEstado()
	{
		return _estado;
	}

	public void SetEstado(string estado)
	{
		_estado = estado;
	}

	public List<ObjBloque> GetAdyacentes()
	{
		return _adyacentes;
	}

	/// <summary>
	/// Bloque para formar figura
	/// </summary>
	/// <param name="bloque">GameObject del bloque</param>
	/// <param name="fila">Fila a la que pertenece</param>
	/// <param name="columna">columna a la que pertenece</param>
	/// <param name="estado">estado actual del objeto</param>
	public ObjBloque(GameObject bloque, string estado)
	{
		_bloque = bloque;
		_estado = estado;
		_adyacentes = new List<ObjBloque>();
	}
}
