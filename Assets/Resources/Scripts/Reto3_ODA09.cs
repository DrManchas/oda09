﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Reto3_ODA09 : MonoBehaviour {

	//Scripts ayuda
	GenericUI_ODA09 genericUIScript;
	Menu_ODA09 menuScript;


	//Pantallas
	List<GameObject> escenas = new List<GameObject>();

	//BlockScreens
	GameObject imgBlockScreen1, imgBlockScreen4;

	//popUps
	GameObject popUpPizarron;

	//Botones popUp
	Button btnNo, btnSi;

	//ObjetosDeReto
	GameObject figura, jugador;
	ObjBloque bloqueActual;
	List<ObjBloque> bloques = new List<ObjBloque>();

	//Numeros usados al generar piramide
	int numSec = 0;
	int numInc = 0;

	//Variables reto
	int escenaActual = 0;

	void Awake(){

		//Scripts de Ayuda
		genericUIScript = this.transform.GetComponent<GenericUI_ODA09>();
		menuScript = this.transform.GetComponent<Menu_ODA09>();


	}

	// Use this for initialization
	void Start ()
	{

		//Escenas
		escenas.Add(menuScript.retos[2].transform.FindChild ("Escena1").gameObject);
		escenas.Add(menuScript.retos[2].transform.FindChild ("Escena2").gameObject);
		escenas.Add(menuScript.retos[2].transform.FindChild ("Escena3").gameObject);
		escenas.Add(menuScript.retos[2].transform.FindChild ("Escena4").gameObject);

		//BlockScreens
		imgBlockScreen1 = escenas[0].transform.FindChild ("imgBlockScreen").gameObject;
		imgBlockScreen4 = escenas[3].transform.FindChild ("imgBlockScreen").gameObject;

		//popUps
		popUpPizarron = imgBlockScreen4.transform.FindChild ("popUpPizarron").gameObject;

		//Botones
		btnNo = popUpPizarron.transform.FindChild("btnNo").GetComponent<Button>();
		btnSi = popUpPizarron.transform.FindChild("btnSi").GetComponent<Button>();

		//Funciones Botones
		btnNo.onClick.AddListener (delegate() {
			genericUIScript.irMenu();
			closePopUp("popUpPizarron");
		});
		btnSi.onClick.AddListener (delegate() {
			StartCoroutine(secuenciaEventos("avanzarEscena_5",1.2f));
			closePopUp("popUpPizarron");
		});
	}

	void Update(){

		//Posicion del jugador
		if (jugador != null && bloqueActual != null) 
		{
			float posicionX = 0.0f;
			if (figura.name == "camino1") {
				posicionX = -bloqueActual.GetBloque ().transform.localPosition.x;
			} else {
				posicionX = bloqueActual.GetBloque().transform.localPosition.x-20;
			}

			jugador.transform.localPosition = new Vector3(
				posicionX,
				bloqueActual.GetBloque().transform.localPosition.y+ 140.0f,
				3.0f
			);
		}
		//Cubo de ayuda
		if(menuScript.retos[2].activeSelf && genericUIScript.ayuda == "solicitada"){
			genericUIScript.ayuda = "usada";
			mostrarSiguiente ();
		}

	}

	// ------------------------Genericos Retos----------------------

	//Funciones secuencia
	public IEnumerator secuenciaEventos (string secuencia, float tiempoEspera)
	{
		yield return new WaitForSeconds (tiempoEspera);
		string[] instrucciones = secuencia.Split ('_');
		int escena;
		switch (instrucciones[0]) {
		case "openPopUp":
			openPopUp (instrucciones[1]);
			break;
		case "closePopUp":
			closePopUp (instrucciones[1]);
			break;
		case "avanzarEscena":
			if (int.TryParse (instrucciones [1], out escena)) {
				avanzarEscena (escena);
			} else {
				menuScript.abrirMenu ();
			}
			break;
		case "reiniciarEscena":
			if (int.TryParse (instrucciones [1], out escena)) {
				genericUIScript.reiniciarEscena(3,escena);;
			} else {
				genericUIScript.irMenu();
			}
			break;
		}
	}

	//Eventos al  avanzarEscena
	public void avanzarEscena (int escena)
	{

		genericUIScript.ayuda = "disponible";
		genericUIScript.time = 0.0f;
		genericUIScript.paused = true;
		escenaActual = escena;

		imgBlockScreen1.SetActive (false);
		imgBlockScreen4.SetActive (false);

		if (figura != null) {
			limpiarFigura ();
		}

		switch (escena) {
		case 1:
			cerrarOtrasEscenas ();
			genericUIScript.gameUI.SetActive (true);
			StartCoroutine (secuenciaEventos ("openPopUp_instruccionesReto", 1.2f));
			int randomCamino = Random.Range (1, 3);
			figura = escenas [0].transform.FindChild ("camino" + randomCamino).gameObject;
			figura.SetActive (true);
			jugador = escenas [0].transform.FindChild ("imgJugador").gameObject;
			if (randomCamino == 1) {
				obtenerCamino1 ();
			} else {
				obtenerCamino2 ();
			}
			generarCamino ();
			break;
		case 2:
			cerrarOtrasEscenas ();
			genericUIScript.gameUI.SetActive (false);
			StartCoroutine (secuenciaEventos ("openPopUp_escena2", 3.0f));
			break;
		case 3:
			cerrarOtrasEscenas ();
			genericUIScript.gameUI.SetActive (true);
			StartCoroutine (secuenciaEventos ("openPopUp_instruccionesReto", 1.2f));
			int randomCamino2 = Random.Range (1, 3);
			figura = escenas [2].transform.FindChild ("camino" + randomCamino2).gameObject;
			figura.SetActive (true);
			jugador = escenas [2].transform.FindChild ("imgJugador").gameObject;
			if (randomCamino2 == 1) {
				obtenerCamino1 ();
			} else {
				obtenerCamino2 ();
			}
			generarCamino ();
			break;
		case 4:
			cerrarOtrasEscenas ();
			genericUIScript.gameUI.SetActive (false);
			genericUIScript.soundManager.playSound ("FuegosArtificiales",0);
			StartCoroutine (secuenciaEventos("openPopUp_terminarReto", 3.0f));
			break;
		case 5:
			menuScript.abrirReto (4);
			break;
		}
	}

	//Eventos al abrir popUp

	public void openPopUp (string popUpName)
	{

		switch(popUpName){
		case "instruccionesReto":
			genericUIScript.mostrarInfoPopUp (
				"Toca los números que continúan la sucesión para poder liberar la última pista que te llevará a Cubo.",
				"closePopUp_instruccionesReto",
				3, 
				1.2f
			);
			break;
		case "perderJuego":
			limpiarFigura ();
			genericUIScript.mostrarInfoPopUp (
				"Éste no es el camino, vuelve a intentarlo. Ya casi encontramos a Cubo. ",
				"closePopUp_perderJuego",
				3, 
				1.2f
			);
			break;
		case "ganarJuegoE1":
			genericUIScript.paused = true;
			imgBlockScreen1.SetActive (true);
			limpiarFigura ();
			genericUIScript.soundManager.playSound ("ganar_pista",1);
			StartCoroutine (secuenciaEventos("closePopUp_popUpHuella", 2.0f));
			break;
		case "ganarJuegoE3":
			genericUIScript.paused = true;
			limpiarFigura ();
			StartCoroutine (secuenciaEventos("avanzarEscena_4", 1.2f));
			break;
		case "escena2":
			genericUIScript.mostrarInfoPopUp (
				"Esta huella es la de Cubo. Al resolver la última sucesión vamos a dar con él. ¡Intenta superar tu récord!",
				"avanzarEscena_3",
				3,
				1.2f
			);
			break;
		case "terminarReto":
			imgBlockScreen4.SetActive (true);
			break;
		}

		genericUIScript.soundManager.playSound ("Sound_OpenPopUp_10",1);

	}

	//Eventos al cerrar popUp
	void closePopUp (string popUpName)
	{
		switch(popUpName){
		case "instruccionesReto":
			genericUIScript.time = 0.0f;
			genericUIScript.paused = false;
			bloqueActual.GetBloque().transform.GetComponent<Animator>().SetInteger("estado", 2);
			//Habilita siguientes
			foreach (ObjBloque siguientes in bloqueActual.GetAdyacentes()){
				siguientes.GetBloque().transform.GetComponent<Button> ().interactable = true;
			}
			break;
		case "perderJuego":
			genericUIScript.reiniciarEscena(3,escenaActual);
			break;
		case "popUpHuella":
			imgBlockScreen1.SetActive (false);
			avanzarEscena (2);
			break;
		case "popUpPizarron":
			imgBlockScreen4.SetActive (false);
			break;
		}

		genericUIScript.soundManager.playSound ("Sound_Cerrar_3",1);

	}

	void cerrarOtrasEscenas()
	{
		for (int index = 0; index < escenas.Count; index++) {
			if (index + 1 == escenaActual) {
				escenas[index].SetActive (true);
			} else {
				escenas[index].SetActive (false);
			}
		}
	}

	// ------------------------Individuales Reto----------------------

	void obtenerCamino1(){

		//obtiene camino
		for(int index =1 ; index <= figura.transform.childCount; index++){
			bloques.Add (new ObjBloque (figura.transform.FindChild("bloque"+index).gameObject, ""));
		}

		bloques[0].GetAdyacentes().Add(bloques[1]);
		bloques[0].GetAdyacentes().Add(bloques[2]);

		bloques[1].GetAdyacentes().Add(bloques[3]);

		bloques[2].GetAdyacentes().Add(bloques[3]);

		bloques[3].GetAdyacentes().Add(bloques[4]);
		bloques[3].GetAdyacentes().Add(bloques[5]);

		bloques[4].GetAdyacentes().Add(bloques[6]);

		bloques[5].GetAdyacentes().Add(bloques[6]);

		bloques[6].GetAdyacentes().Add(bloques[7]);
		bloques[6].GetAdyacentes().Add(bloques[8]);

		bloques[7].GetAdyacentes().Add(bloques[9]);

		bloques[8].GetAdyacentes().Add(bloques[9]);

	}

	void obtenerCamino2(){

		//obtiene camino
		for(int index =1 ; index <= figura.transform.childCount; index++){
			bloques.Add (new ObjBloque (figura.transform.FindChild("bloque"+index).gameObject, ""));
		}

		bloques[0].GetAdyacentes().Add(bloques[1]);
		bloques[0].GetAdyacentes().Add(bloques[2]);

		bloques[1].GetAdyacentes().Add(bloques[3]);

		bloques[2].GetAdyacentes().Add(bloques[3]);

		bloques[3].GetAdyacentes().Add(bloques[4]);
		bloques[3].GetAdyacentes().Add(bloques[7]);

		bloques[4].GetAdyacentes().Add(bloques[5]);

		bloques[5].GetAdyacentes().Add(bloques[6]);

		bloques[6].GetAdyacentes().Add(bloques[10]);

		bloques[7].GetAdyacentes().Add(bloques[8]);

		bloques[8].GetAdyacentes().Add(bloques[9]);

		bloques[9].GetAdyacentes().Add(bloques[10]);

		bloques[10].GetAdyacentes().Add(bloques[11]);
		bloques[10].GetAdyacentes().Add(bloques[12]);

		bloques[11].GetAdyacentes().Add(bloques[13]);

		bloques[12].GetAdyacentes().Add(bloques[13]);

		bloques[13].GetAdyacentes().Add(bloques[14]);

		bloques[14].GetAdyacentes().Add(bloques[15]);
		bloques[14].GetAdyacentes().Add(bloques[16]);

		bloques[15].GetAdyacentes().Add(bloques[17]);

		bloques[16].GetAdyacentes().Add(bloques[17]);

	}

	void limpiarFigura(){

		foreach (ObjBloque bloq in bloques) {
			bloq.GetBloque ().transform.GetChild (0).GetComponent<Text> ().text = "";
			bloq.GetBloque ().transform.GetComponent<Animator> ().SetInteger ("estado", 0);
		}
		bloques.Clear ();
		bloqueActual = null;
		figura.SetActive (false);
	}

	void generarCamino(){

		try{
			//Secuencia random
			int numNextSec = Random.Range(2,10);
			while (numNextSec == numSec) {
				numNextSec = Random.Range(2,10);
			}
			numSec = numNextSec;

			//Secuencia random
			int numNextInc = Random.Range(5,21);
			while (numNextInc == numInc) {
				numNextInc = Random.Range(5,21);
			}

			numInc = Mathf.Clamp((numNextInc - numSec),3,18);

			//Asigna primer número
			asignarCaminoCorrecto(bloques[0],1);

			//Habilitar primer bloque
			bloqueActual = bloques[0];
		} catch {
			Debug.Log ("No se pudo generar el camino");
		}

	}

	void asignarCaminoCorrecto(ObjBloque actual, int numBloque)
	{
		try{
			//Asigna el valor de la secuencia
			actual.GetBloque().transform.GetChild(0).GetComponent<Text> ().text = ""+(numBloque*numSec + numInc);

			//Agrega funcion de perder al boton correcto
			actual.GetBloque().transform.GetComponent<Button>().onClick.RemoveAllListeners ();
			actual.GetBloque().transform.GetComponent<Button>().onClick.AddListener(delegate() {
				seleccionarSecuencia(actual);
			});
			actual.GetBloque ().transform.GetComponent<Button> ().interactable = false;

			//Estado de número de secuencia correcto
			actual.SetEstado ("correcto");

			//Continua asignando si no es el final de la piramide
			if(actual.GetAdyacentes ().Count != 0){
				numBloque++;
				int randomSiguiente = Random.Range (0, actual.GetAdyacentes ().Count);

				asignarCaminoCorrecto (actual.GetAdyacentes () [randomSiguiente], numBloque);
				for (int index = 0; index < actual.GetAdyacentes ().Count; index++) {
					if (index != randomSiguiente) {
						if (actual.GetAdyacentes () [index].GetEstado () != "correcto") {
							asignarCaminoInCorrecto (actual.GetAdyacentes () [index], numBloque);
						}
					}
				}
			}
		} catch {
			Debug.Log ("No se pudo asignar el camino correcto");
		}
	}

	void asignarCaminoInCorrecto(ObjBloque actual, int numBloque)
	{

		try{
			int randomSum = Random.Range (-1, 1);
			int multiplicacion = numBloque*numSec + numInc;
			//Asegurar que número no este en la piramide
			while (randomSum == 0){
				randomSum = Random.Range (-1, 1);
			}

			//Asignar valor aleatorio parecido al anterior
			actual.GetBloque().transform.GetChild(0).GetComponent<Text> ().text = ""+
				(Mathf.Clamp((multiplicacion+randomSum),1,(multiplicacion+randomSum)+1));

			//Estado de número de secuencia incorrecto
			actual.SetEstado ("incorrecto");

			//Agrega funcion de perder al boton incorrecto
			actual.GetBloque().transform.GetComponent<Button>().onClick.AddListener(delegate() {
				seleccionarSecuencia(actual);
			});
			actual.GetBloque ().transform.GetComponent<Button> ().interactable = false;

			for (int index = 0; index < actual.GetAdyacentes ().Count; index++) {
				if (actual.GetAdyacentes () [index].GetEstado () != "correcto") {
					asignarCaminoInCorrecto (actual.GetAdyacentes () [index], numBloque+1);
				}
			}
		} catch {
			Debug.Log ("No se pudo asignar el camino incorrecto");
		}

	}

	void seleccionarSecuencia(ObjBloque bloquePresionado)
	{
		try{
			int numBloque;
			int.TryParse (bloquePresionado.GetBloque().transform.GetChild(0).GetComponent<Text> ().text, out numBloque);
			if ((numBloque-numInc) % numSec == 0) {
				bloqueActual = bloquePresionado;
				bloqueActual.GetBloque().transform.GetComponent<Animator>().SetInteger("estado", 2);
				bloqueActual.GetBloque ().transform.GetComponent<Button> ().interactable = false;
				//Jugador gana
				if (bloquePresionado.GetAdyacentes ().Count == 0) {
					StartCoroutine( secuenciaEventos (("openPopUp_ganarJuegoE"+escenaActual),1.2f));
				} else {
					//Habilita siguientes
					foreach (ObjBloque siguiente in bloquePresionado.GetAdyacentes()){
						if(siguiente.GetBloque().transform.GetComponent<Animator>().GetInteger("estado") != 2){
							siguiente.GetBloque().transform.GetComponent<Button> ().interactable = true;
						}
					}
				}
				genericUIScript.soundManager.playSound ("Sound_correcto_10",1);
			} else {
				genericUIScript.soundManager.playSound ("Sound_incorrecto_19",1);
				StartCoroutine(secuenciaEventos ("openPopUp_perderJuego",1.2f));
			}
		} catch {
			Debug.Log ("No se pudo seleccionar el siguiente numero de la secuencia");
		}
	}

	void mostrarSiguiente(){
		try{
			int numBloque;
			for (int index = 0; index < bloqueActual.GetAdyacentes().Count; index++) {
				int.TryParse (bloqueActual.GetAdyacentes () [index].GetBloque().transform.GetChild(0).GetComponent<Text> ().text, out numBloque);
				if ((numBloque-numInc) % numSec == 0) {
					bloqueActual.GetAdyacentes () [index].GetBloque().transform.GetComponent<Animator>().SetInteger("estado", 1);
					break;
				}
			}
		} catch {
			Debug.Log ("No se pudo mostrar el numero siguiente");
		}
	}
}
