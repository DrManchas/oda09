﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RetoFinal_ODA09 : MonoBehaviour {

	//Scripts ayuda
	GenericUI_ODA09 genericUIScript;
	Menu_ODA09 menuScript;


	//popUps
	List<GameObject> popUps = new List<GameObject>();

	//popUpsButtons
	Button btnAtras, btnAceptar, btnSalir;
	List<Button> btnRespuestas = new List<Button>();

	//popUpsText
	Text txtPregunta;
	List<Text> txtRespuestas = new List<Text>();

	Text txtCalificacion;
	GameObject respuestaCorrecta;
	GameObject respuestaIncorrecta;

	//Variables de evaluacion
	List<Question> preguntas = new List<Question>();
	List<Answer> respuestas = new List<Answer>();
	int preguntaActual = 0;

	bool iniciado = false;


	//Colores preguntas
	Color answerSettedColor = new Color32(14,209,242,255);
	Color defaultAnswerColor = new Color32(9,74,139,255);

	// ------------------------Genericos Unity----------------------
	void Start ()
	{

		//Scripts de Ayuda
		genericUIScript = this.transform.GetComponent<GenericUI_ODA09>();
		menuScript = this.transform.GetComponent<Menu_ODA09>();

		//popUps
		popUps.Add(menuScript.retos[3].transform.FindChild ("popUpPregunta").gameObject);
		popUps.Add(menuScript.retos[3].transform.FindChild ("popUpResultados").gameObject);

		//popUps Buttons
		btnAtras = popUps[0].transform.FindChild("btnAnterior").GetComponent<Button>();
		btnAceptar = popUps [0].transform.FindChild ("btnAceptar").GetComponent<Button> ();

		btnSalir = popUps[1].transform.FindChild("btnSalir").GetComponent<Button>();

		GameObject goRespuestas = popUps [0].transform.FindChild ("respuestas").gameObject;
		btnRespuestas.Add (goRespuestas.transform.FindChild("btnRespuesta1").GetComponent<Button>());
		btnRespuestas.Add (goRespuestas.transform.FindChild("btnRespuesta2").GetComponent<Button>());
		btnRespuestas.Add (goRespuestas.transform.FindChild("btnRespuesta3").GetComponent<Button>());

		//popUps Text
		txtPregunta = popUps[0].transform.FindChild("txtPregunta").GetComponent<Text>();

		txtRespuestas.Add (btnRespuestas[0].transform.GetChild(0).GetComponent<Text>());
		txtRespuestas.Add (btnRespuestas[1].transform.GetChild(0).GetComponent<Text>());
		txtRespuestas.Add (btnRespuestas[2].transform.GetChild(0).GetComponent<Text>());

		txtCalificacion = popUps[1].transform.FindChild("txtCalificacion").GetComponent<Text>();
		respuestaCorrecta = popUps[1].transform.FindChild("imgCorrecto").gameObject;
		respuestaIncorrecta = popUps[1].transform.FindChild("imgIncorrecto").gameObject;

		//Funciones Botones
		btnAtras.onClick.AddListener (delegate() {
			regresar();
		});
		btnAceptar.onClick.AddListener (delegate() {
			avanzar();
		});
			
		btnSalir.onClick.AddListener (delegate() {
			limpiarResultados ();
			genericUIScript.irMenu();
		});
			
		btnRespuestas[0].onClick.AddListener (delegate() {
			seleccionarRespuesta(0);
		});
		btnRespuestas[1].onClick.AddListener (delegate() {
			seleccionarRespuesta(1);
		});
		btnRespuestas[2].onClick.AddListener (delegate() {
			seleccionarRespuesta(2);
		});
		//mostrarPregunta ();
	}
		
	void Update(){
		if (menuScript.retos [3].activeSelf && !iniciado) {
			StartCoroutine(secuenciaEventos("iniciarEvaluacion",0.0f));
			iniciado = true;
		}
	}

	// ------------------------Genericos Retos----------------------

	//Funciones secuencia
	public IEnumerator secuenciaEventos (string secuencia, float tiempoEspera)
	{
		yield return new WaitForSeconds (tiempoEspera);
		switch (secuencia) {
		case "iniciarEvaluacion":
			genericUIScript.soundManager.playSound ("Sound_Fondoevaluacion", 0);
			crearCuestionario ();
			seleccionarPreguntasRandom (5);
			preguntaActual = 0;
			popUps [0].SetActive (true);
			popUps [1].SetActive (false);
			limpiarResultados ();
			mostrarPregunta ();
			break;
		case "evaluar":
			mostrarResultados ();
			break;
		case "salir":
			limpiarResultados ();
			genericUIScript.irMenu ();
			break;
		}

	}

	// ------------------------Funciones UI------------------------------
	void mostrarPregunta(){
		txtPregunta.text = ""+(preguntaActual+1)+". "+respuestas [preguntaActual].GetQuestion ().GetQuestionText ();
		txtRespuestas [0].text = "a) "+respuestas [preguntaActual].GetQuestion ().GetAnserTexts () [0];
		txtRespuestas [1].text = "b) "+respuestas [preguntaActual].GetQuestion ().GetAnserTexts () [1];
		txtRespuestas [2].text = "c) "+respuestas [preguntaActual].GetQuestion ().GetAnserTexts () [2];

		colorearRespuestaSeleccionada ();

		if (preguntaActual <= 0) {
			btnAtras.interactable = false;
		} else {
			btnAtras.interactable = true;
			btnAtras.transform.GetChild (0).GetComponent<Text> ().text = "ANTERIOR";
		}

		if (preguntaActual >= respuestas.Count-1) {
			btnAceptar.transform.GetChild (0).GetComponent<Text> ().text = "TERMINAR";
		} else {
			btnAceptar.transform.GetChild (0).GetComponent<Text> ().text = "SIGUIENTE";
		}
	}

	void colorearRespuestaSeleccionada()
	{
		for (int index = 0; index < txtRespuestas.Count; index++) 
		{
			if (index == respuestas [preguntaActual].GetAnswer ()) {
				txtRespuestas[index].color = answerSettedColor;
				//btnRespuestas[index].image.color = answerSettedColor;
			} else {
				txtRespuestas[index].color = defaultAnswerColor;
				//btnRespuestas[index].image.color = defaultAnswerColor;
			}
		}
	}

	void mostrarResultados(){
		popUps [0].SetActive (false);
		popUps [1].SetActive (true);

		int correctas = 0;
		respuestaCorrecta.SetActive(true);
		respuestaIncorrecta.SetActive(true);
		for (int index = 0; index < respuestas.Count; index++) {
			GameObject imgRespuestaClone;


			if (respuestas [index].GetQuestion ().ReviewQuestion (respuestas [index].GetAnswer ())) {
				imgRespuestaClone = (GameObject) Instantiate(respuestaCorrecta);
				imgRespuestaClone.transform.SetParent(popUps [1].transform);
				imgRespuestaClone.transform.localScale = new Vector3(1, 1, 1);
				imgRespuestaClone.transform.localPosition = respuestaCorrecta.transform.localPosition + (Vector3.down * index * 34);
				correctas++;
			} else {
				imgRespuestaClone = (GameObject) Instantiate(respuestaIncorrecta);
				imgRespuestaClone.transform.SetParent(popUps [1].transform);
				imgRespuestaClone.transform.localScale = new Vector3(1, 1, 1);
				imgRespuestaClone.transform.localPosition = respuestaIncorrecta.transform.localPosition + (Vector3.down * index * 34);
			}
			imgRespuestaClone.name = "resultado";
		}
		respuestaCorrecta.SetActive(false);
		respuestaIncorrecta.SetActive(false);
		txtCalificacion.text = "" + (correctas * 2) + " / 10";
	}

	void limpiarResultados(){
		foreach (Transform child in popUps[1].transform) {
			if (child.name == "resultado") {
				Destroy (child.gameObject);
			}
		}
	}

	// ------------------------Funciones Botones-------------------------

	void regresar()
	{
		preguntaActual--;
		mostrarPregunta ();
	}

	void avanzar()
	{
		if (preguntaActual >= respuestas.Count-1)
		{
			StartCoroutine (secuenciaEventos("evaluar",0.1f));
		}
		else
		{
			preguntaActual++;
			mostrarPregunta ();
		}
	}

	void seleccionarRespuesta(int boton)
	{
		respuestas [preguntaActual].SetAnswer (boton);
		genericUIScript.soundManager.playSound ("Sound_SeleccionarRespuesta",1);

		colorearRespuestaSeleccionada ();

	}

	// ------------------------Funciones Evaluacion----------------------

	void seleccionarPreguntasRandom(int numeroPreguntas)
	{

		respuestas = new List<Answer> ();
		for (int i = 0; i < numeroPreguntas; i++)
		{
			int randomIndex = Random.Range(0, preguntas.Count);
			respuestas.Add(new Answer(preguntas[randomIndex], -1));
			preguntas.RemoveAt(randomIndex);
		}
	}

	void crearCuestionario()
	{

		preguntas = new List<Question>();
		preguntas.Add(new Question("¿Qué número representa la sucesión aritmética que hay en los siguientes elementos: 3, 7, 11,15, 19..?",
			new string[] { "3", "4", "2" },
			1));
		preguntas.Add(new Question("¿Qué número representa la sucesión aritmética que hay en los siguientes elementos: 20, 42, 64, 86..?",
			new string[] { "18", "20", "22" },
			2));
		preguntas.Add(new Question("¿Qué número representa la sucesión aritmética que hay en los siguientes elementos: 20, 16, 12, 8..?",
			new string[] { "6", "2", "4" },
			2));
		preguntas.Add(new Question("Completa el término que falta en la sucesión: 8, 10, 12, __, 16",
			new string[] { "14", "10", "18" },
			0));
		preguntas.Add(new Question("Completa el término que falta en la sucesión: 35,__, 25, 20, 15",
			new string[] { "40", "30", "45" },
			1));
		preguntas.Add(new Question("Completa el término que falta en la sucesión: 6, 12,__ ,24 ,30",
			new string[] { "14", "10", "18" },
			2));
	}

}

public class Question
{
	private string _questionText;
	private string[] _answersTexts;
	private int _correctAnswer;

	/// <summary>
	/// Question with only one correct answer
	/// </summary>
	/// <param name="questionTest">Here comes the question string</param>
	/// <param name="answerTexts">Here comes all the text of each answer</param>
	/// <param name="correctAnswer">Correct Answer, NOTE it has to be int starting in 0</param>
	public Question(string questionTest, string[] answerTexts, int correctAnswer)
	{
		_questionText = questionTest;
		_answersTexts = answerTexts;
		_correctAnswer = correctAnswer;
	}

	public bool ReviewQuestion(int answer)
	{
		if (answer == _correctAnswer) {
			return true;
		} else {
			return false;
		}
	}

	public string GetQuestionText()
	{
		return _questionText;
	}

	public string[] GetAnserTexts()
	{
		return _answersTexts;
	}
		
}

public class Answer
{
	private Question _question;
	private int _answer;

	/// <summary>
	/// Answer for a single answer question
	/// </summary>
	/// <param name="question">Question object</param>
	/// <param name="answer">Answer to the question object</param>
	public Answer(Question question, int answer)
	{
		_question = question;
		_answer = answer;
	}

	public Question GetQuestion()
	{
		return _question;
	}

	public int GetAnswer()
	{
		return _answer;
	}

	public void SetAnswer(int answer)
	{
		_answer = answer;
	}
}
